		�������  ��  ������������  ������ N 1

1) ������������� � �ࠢ����� �믮������ ��������� ࠡ�� (䠩�
RULES.TXT).

2) � ����������� EGA (640�350) � ���������� �� 䮭� ��� �㫥� �
���� � ���ᮬ A1h �⭮�⥫쭮 ��砫� ���������� ����ᠭ ��� 0Ah, �
� ᮮ⢥�����騥 ��������᪮�� ��� 1001B.  ��।�����, ��
�⮡ࠧ���� �� ��࠭� � �⮬ ��砥, � ���᭮����� �⮣� ��।������
�।�⠢��� �९�����⥫�.
���ࠡ���� �ணࠬ��, �⮡ࠦ����� �� �� ��࠭�.

3) ���ࠡ���� �ணࠬ�� ��� �⮣� �� ����������� ��� �뢮�� �� ��࠭
�窨 � ���न��⠬� x = 25, y = 151, 梥⮬ 1100B. ���祭�� 梥� �
���न��� ����� �������� �����।�⢥��� � ⥪�� �ணࠬ��.

4) �� ���� ࠧࠡ�⠭��� �ணࠬ�� �뢮�� �窨 ࠧࠡ���� �ணࠬ��
����஥��� ��ਧ��⠫쭮� ����� �ந����쭮�� ࠧ��� �� �������쭮�� (�
���� ���ᥫ�) �� ���ᨬ��쭮�� (640 ���ᥫ��). ��६����� �
���ᨬ��쭮� ᪮��� ����஥��� �����.

5) �� ���� ࠧࠡ�⠭��� �ணࠬ�� �뢮�� �窨 ࠧࠡ���� �ணࠬ��
����஥��� ���⨪��쭮� ����� �ந����쭮�� ࠧ��� �� �������쭮�� (�
���� ���ᥫ�) �� ���ᨬ��쭮��. ��६����� � ���ᨬ��쭮�
᪮��� ����஥��� �����.

6) ������� � �९�����⥫� ������� �� ����஥��� 䨣��� �� ���� ��楤�� 
����஥��� ��ਧ��⠫쭮� � ���⨪��쭮� �����.