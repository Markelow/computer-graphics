.model small
.stack 128

.data
  helloMessage db 10,13,"Enter number, '+', '-', 'p' or 'q'",10,13,"$"
  inputErrorLabel db 10,13,"Incorrect value. Repeat!", 10,13,"$"
  calcErrorLabel db 10,13,"There are not enough operands.",10,13,"$"
  countOfNumber db 0
  newLine db 10,13,"$"
  currentResult db "Current result = $"
  
.code
  
  main:
    mov ax,@data
	mov ds,ax
	mov cx,0
	lea dx,helloMessage
	mov ah,09h
	int 21h
	
	read:
		mov ah,01h
		int 21h
		cmp al,'q'
		je close
		cmp al,'p'
		je showResult
		cmp al,'+'
		je addOperands
		cmp al,'-'
		je subOperands
		cmp al,'0'
		jge getNumber
		jmp read
		 
	close:
		mov sp, 0  
		mov ax,4c00h 
		int 21h
		
	showResult:
		cmp cx,0
		je printCalcError
		call goToNewLine	
		lea dx, currentResult
		mov ah,09h
		int 21h
		xor dx,dx
		pop ax
		push ax
		cmp ax,0
		js getNegative
		jmp covnertToDicimal
		getNegative:
			mov bx,ax
			neg bx 
			mov ah,02h
			mov dl,'-'
			int 21h
			mov ax,bx
		covnertToDicimal:
			mov bx,10
		divide:
			and dx,0
			div bx
			push dx
			inc countOfNumber
			cmp ax,0
			jne divide
		printDecimal:
			pop dx
			add dl,'0'
			mov ah,02h
			int 21h
			dec countOfNumber
			jne printDecimal
		call goToNewLine
		jmp read	
	
	goToNewLine proc
		lea dx, newLine
		mov ah,09h
		int 21h
		ret
	goToNewLine endp
	
	addOperands:
	    cmp cx,1
		jbe printCalcError
		pop ax
		pop bx
		add ax,bx
		push ax
		dec cx
		call goToNewLine
		jmp read
	
	subOperands:
	    cmp cx,1
		jbe printCalcError
		pop bx
		pop ax
		sub ax,bx
		push ax
		dec cx
		call goToNewLine
		jmp read
	
	getNumber:
		cmp al,'9'
		jg printInputError
		sub al,'0'
		and ah,0
		push ax
		inc cx
		call goToNewLine
		jmp read
	
	printCalcError:
		lea dx, calcErrorLabel
		mov ah,09h
		int 21h
		jmp read
		
	printInputError:
		lea dx, inputErrorLabel
		mov ah,09h
		int 21h
		jmp read	
		
		
  end main