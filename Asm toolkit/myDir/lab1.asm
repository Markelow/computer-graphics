.model tiny
CSEG segment
org 100h

begin:
 xor cx,cx
 xor ax,ax
 int 10h

 read:
  mov ah,08h
  int 21h
  call clr
  call showal
  cmp al,'p'
  je show
  cmp al,'q'
  je endl
  cmp al,'+'
  je plus
  cmp al,'-'
  je minus
  cmp al,'0'
  jge addst
 readcl:
  call clr
  jmp read

 minus:
  cmp cx,1
  jbe errsize
  pop bx
  pop ax
  sub ax,bx 
  push ax
  dec cx
  jmp read 

 plus:
  cmp cx,1
  jbe errsize
  pop ax
  pop bx
  add ax,bx
  push ax
  dec cx
  jmp read

 errsize:
  call clr
  lea dx,errorsize
  mov ah,09h
  int 21h
  jmp read

 addst:
  cmp al,'9'
  jg readcl
  sub al,'0'
  xor ah,ah
  push ax
  inc cx
  jmp read

 show:
  cmp cx,0
  je err
  xor dx,dx
  mov ah,02h
  int 10h
  pop ax
  push ax
  cmp ax,0
  js nega
  jmp divvpr
  nega:
   mov bx,ax
   neg bx 
   mov ah,02h
   mov dl,'-'
   int 21h
   mov ax,bx
  divvpr:
   push cx
   xor cx,cx
   mov bx,10
  divv:
   xor dx,dx
   cwd
   div bx
   push dx
   inc cx
   cmp ax,0
   jne divv
  divv2:
   pop dx
   add dl,'0'
   mov ah,02h
   int 21h
   dec cx
   jne divv2
   pop cx
   jmp read

  err:
   call clr
   lea dx,error
   mov ah,09h
   int 21h
   jmp read

  endl:
   xor ax,ax
   int 21h
	
 clr proc
  xor bx,bx
  xor dx,dx
  mov ah,02h
  int 10h
  mov ah,09h
  lea dx,empty
  int 21h
  xor dx,dx
  mov ah,02h
  int 10h
  ret
 clr endp

 showal proc
  mov ah,02h
  mov dl,al
  int 21h
  ret
 showal endp
	
error db "Stack empty$"
errorsize db "Not enough elements$"
empty db "                                        $"
CSEG ends
end begin
