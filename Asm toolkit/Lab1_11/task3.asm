.286
.model  small, C
.stack 256
.data
	leftByteMask  db 255,127,63,31,15,7,3,1
	rightByteMask db 10000000b
				  db 11000000b
				  db 11100000b
				  db 11110000b
				  db 11111000b
				  db 11111100b
				  db 11111110b
				  db 11111111b
				  
   	color 	 equ 1100b		;код цвета - ярко-красный
	twoBytes equ 0x01
	oneByte  equ 0h
.code

;Строит горизонтальную линию
draw_hor_line proc x1:word, x2:word, y:word
	
	; проверка аргументов на корректность
	mov ax,x1
	mov bx,x2
	cmp bx,ax
	jl exchange
	jmp correctArgs
	exchange:
		mov ax, x1
		mov bx, x2
		mov x1, bx
		mov x2, ax
	correctArgs:
	
	xor ax,ax
	xor bx,bx
		
	;---установка регистра маски карты для цвета 1101b
	mov dx,03c4h       	;указываем на адресный регистр
	mov al,2           	;индекс регистра маски карты
	out dx,al          	;установка адреса
	inc dx             	;указываем на регистр данных
	mov al,color       	;код цвета
	out dx,al          	;посылаем код цвета
	
	;--- определяем байтовое смещение x1
	xor bx, bx	  ; обнуляем регистр адреса	
		
	mov ax, y
	mov dx, x1
	mov	cx, dx			;значение x
	shl	ax,4			; y<<4
	add	bx,ax			; adr+=y<<4
	shl	ax,2			; (y<<4)<<2
	add	bx,ax			; adr+=(y<<4)<<2
	shr	dx,3			; х>>3
	add	bx,dx			; adr+=x>>3
	push bx				; сохраним байтовое смещение x1 
	
	;--- определяем байтовое смещение x2
	xor bx, bx	  ; обнуляем регистр адреса	
		
	mov ax, y
	mov dx, x2
	mov	cx, dx			;значение x
	shl	ax,4			; y<<4
	add	bx,ax			; adr+=y<<4
	shl	ax,2			; (y<<4)<<2
	add	bx,ax			; adr+=(y<<4)<<2
	shr	dx,3			; х>>3
	add	bx,dx			; adr+=x>>3
	
	;-- достаем байтовое смещение x1
	pop ax
	; находим разность, т.е. количество байт
	sub bx, ax
	
	; сохраняем количество байт и начальное смещение для будущего
	push bx 
	push ax 
	
	; определяем, какая у нас ситуация
	jz inSameByte
	sub bx, 01h
	jz twoNearBytes
	jmp moreThanTwo
	
	;--- все точки в одном байте
	inSameByte:			
		mov  dx, 03ceh       	;указываем на адресный регистр
		mov  al, 8           	;номер регистра
		out  dx, al          	;посылаем его
		inc  dx  	        	;указываем на регистр данных
		
		;--- формируем маску
		mov cx, x1
		mov	ah, 10000000b	;маскa
		and cl, 00000111b	;получаем остаток от деления x на 8
		shr ah, cl			;сдвигаем маску на удвоенный остаток
		mov bh, ah
		
		mov dx, x2
		sub dx, x1
		jz onePixel
		
		mov cx, x2
		sub cx, x1
		sameByteMask:
			shr bh,1 
			or ah, bh
			loop sameByteMask
		
		onePixel:
		
		;---маскируем нужные биты
		mov  dx, 03ceh       	;указываем на адресный регистр
		mov  al, 8           	;номер регистра
		out  dx, al          	;посылаем его
		inc  dx  	        	;указываем на регистр данных
		mov  al, ah          	;маска
		out  dx, al          	;посылаем данные
				
		pop  bx
			
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
		
		;---чистим текущее содержимое задвижки
		mov  al,es:[bx]     	;читаем содержимое в задвижку
		mov  al,0           	;готовимся к очистке
		mov  es:[bx],al     	;чистим задвижку
		;---рисуем точку
		mov  al,0ffh        	;значение со всеми установленными битами
		mov  es:[bx],al     	;выводим точки
		add  bx, 050h	   		;переходим на следующую
		

		jmp finish
	;--- точки в двух последовательных байтах
	twoNearBytes:
		
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
	
		pop bx
		
		; загружаем маску
		MOV dx, x1 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, leftByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		inc bx
		
		; загружаем маску
		MOV dx, x2 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, rightByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		jmp finish	
	
	;--- точки в 3 бай и более	
	moreThanTwo:
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
	
		pop bx
		
		;--- закрашиваем левый байт
		MOV dx, x1 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, leftByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		inc bx
				
		;---все центральные байты
		pop cx
		
		mov di, bx ; устанавливаем смещение на начало
		mov al, 0ffh ; будем заполнять
		rep stosb
		
		;закрашиваем правый байт
		mov bx, di
		MOV dx, x2 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, rightByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
	
	finish:
		ret
draw_hor_line endp

main:
	mov ax,@data
	mov ds,ax

	mov ax, 010h
	int 10h

	mov ax,0a000h      ;указываем на видеобуфер
	mov es,ax          ;

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	push 200			;y
	push 020			;x2
	push 0				;x1
	
	push 255			;y
	push 270h			;x2
	push 7				;x1
	call draw_hor_line
	
	push 250			;y
	push 0FH			;x2
	push 7				;x1
	call draw_hor_line
	
	
	push 245			;y
	push 3				;x2
	push 10				;x1
	call draw_hor_line
	
	
	
	xor ax,ax			;ожидание нажатия клавиши
	int 16h

	mov ax,4c00h		;выход из графики с возвратом
	int 21h				;в предыдущий режим
end main