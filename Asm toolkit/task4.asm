.286
.model  small
.stack 256		;устанавливаем размер стека
.data
	color 	equ 1100b		;код цвета - ярко-красный
	x 	  	equ 353
	y1 	  	equ 151
	y2 	  	equ 200
	lngth	equ 100
.code			;сегмент кода

INCLUDE draw_vert_line.asm

main:
   	mov	ah,00h		;инициализация графического
	mov	al,010h    	;режима
	int	10h       	;устанавливаем режим

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим

   	mov  ax,0a000h    ;указываем на видеобуфер
   	mov  es,ax        ;

	xor 	bx, bx			; обнуляем регистр адреса

	call draw_vert_line			;

	xor	ax,ax		;ожидание нажатия клавиши
	int	16h

	mov	ax,04c00h	;выход из графики с возвратом
	int	21h		;в предыдущий режим

end main	