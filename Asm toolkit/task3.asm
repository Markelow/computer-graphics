.model  small
.stack 256
.data
	bitmask db 255,127,63,31,15,7,3,1
   	color 	equ 1100b		;код цвета - ярко-красный
	x1	  	equ 153
	x2 	  	equ 190
	y 	  	equ 200
.code

draw_hor_line proc
;---установка регистра маски карты для цвета 1101b
	mov  dx,03c4h       	;указываем на адресный регистр
	mov  al,2           	;индекс регистра маски карты
	out  dx,al          	;установка адреса
	inc  dx             	;указываем на регистр данных
	mov  al,color       	;код цвета
	out  dx,al          	;посылаем код цвета
	
	mov bx, x1 ;x1
	mov ax, y
	push bx
	mov dx, x2 ;coord x2
	push dx

	mov dx, bx
	and dx, 07h
	mov cl, 3
	shr bx, cl ;bx = x/8

	mov cx, ax
	and cx, 01h
	cmp cx, 0
	jz EV
	add bx, 2000h
	dec ax

EV: 
	mov cl, 1
	shr ax, cl ;ax = (y-1)/2
	mov cl, 4
	sal ax, cl ;ax=((y-1)/2)*2^4, bx=x/8
	add bx, ax
	mov cl, 2
	sal ax, cl ;ax=((y-1)/2)*2^6, bx=x/8 + ((y-1)/2)*2^4
	add bx, ax

	xor ax,ax
	mov si,dx
	mov al,bitmask+[si]  ; в al - маска 

	pop dx ;x2
	pop cx ;x1
	sub dx, cx ;x2-x1
	mov cx, dx 
	and cx, 7
	push cx
	mov cx, 3
	shr dx, cl
	mov cx, dx


	mov es:[bx],ax     ;выводим точку  
	mov al, 255  
 
GR:	mov es:[bx],ax
      inc bx
	loop GR
	
	pop cx
	;mov cx, 7
	;dec cx
	mov ax, 0080h
	mov dx, ax

GR2:
	
	shr ax, 1
	add dx, ax
	loop GR2

	mov es:[bx],dx
	
	ret
draw_hor_line endp

main:
	mov ax,@data
	mov ds,ax

	mov ax, 010h
	int 10h

	mov ax,0a000h      ;указываем на видеобуфер
	mov es,ax          ;

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	call draw_hor_line

	xor ax,ax			;ожидание нажатия клавиши
	int 16h

	mov ax,4c00h		;выход из графики с возвратом
	int 21h				;в предыдущий режим

end main