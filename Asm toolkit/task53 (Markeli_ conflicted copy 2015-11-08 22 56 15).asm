.286
.model  small, c
.stack 256
.data
				  
   	color 	 equ 1100b		;код цвета - ярко-красный
	quadtrant1Mask equ 08h  ;маски для квадрантов
	quadtrant2Mask equ 04h
    quadtrant3Mask equ 02h
    quadtrant4Mask equ 01h
.code

;строит горизонтальную линию
draw_line proc x1:word, x2:word, y1:word, y2:word
	local deltax:word, deltay:word, ystep: word, errorv: word, steep:word, temp:word 
	
	; проверяем рост отрезка по оси икс и по оси игрек
	; var steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
	mov bx,y2
	mov ax,y1
	sub bx,ax
	
	push bx
	call math_abs
	mov bx, ax
	
	mov ax,x2
	mov cx,x1
	sub ax,cx
	
	push ax
	call math_abs
	
	; Отражаем линию по диагонали, если угол наклона слишком большой
	sub bx,ax
	jge y_steep
	jmp x_steep
	y_steep:
		mov steep,1
		mov ax,x1
		mov bx,y1
		mov x1,bx
		mov y1,ax
		mov ax,x2
		mov bx,y2
		mov x2,bx
		mov y2,ax
		jmp no_swap_on_steep
	x_steep:
		mov steep,0
		jmp no_swap_on_steep
	
	no_swap_on_steep:
	
	; // Если линия растёт не слева направо, то меняем начало и конец отрезка местами	
	swap_if_x_incorrect:
		mov ax,x2
		sub ax,x1
		jng swap_on_x_incorrect
		jmp no_swap_on_x_incorrect
		swap_on_x_incorrect:
			mov ax,x1
			mov bx,x2
			mov x1,bx
			mov x2,ax
			mov ax,y1
			mov bx,y2
			mov y1,bx
			mov y2,ax
		no_swap_on_x_incorrect:
		
	; dx
	mov ax,x2
	sub ax,x1
	mov deltax,ax
	
	; dy
	mov ax,y2
	sub ax,y1
	push ax 
	call math_abs
	mov deltay, ax
	
	; Выбираем направление роста координаты y
	mov dx,y1
	sub dx,y2
	jl posit_step
	jmp negate_step
		posit_step:
			mov ystep,1
			jmp loop_prepare
		negate_step:
			mov ystep,-1
	
	loop_prepare:
	
	;x = x1
	mov ax,x1
	; y = y1
	mov bx,y1
	
	mov cx,x2
	sub cx,x1
	
	;error
	mov dx,deltax
	shr dx,1
	mov errorv,dx

	; рисуем
	draw_loop:
		mov dx, steep
		cmp dx,1
		je high_steep
		jmp low_steep
		high_steep:
			push ax
			push bx
			call draw_point
			jmp recalc
		low_steep:
			push bx
			push ax
			call draw_point
		recalc:
		; корректировка
		mov dx, errorv
		sub dx, deltay
		mov errorv,dx
		cmp dx,0
		jl fix_error
		jmp no_error
		fix_error:	
			add bx, ystep
			mov dx,errorv
			add dx,deltax
			mov errorv,dx
			mov dx,ax
			
		no_error:
		
		inc ax
	loop draw_loop;

	finish:
		ret 
draw_line endp

math_abs proc num:word

	mov ax, num
	abs:
	neg ax
	js abs ; переход на метку если выставлен флаг sf (то есть если число отрицательное)
		
	ret 
math_abs endp

draw_circle proc uses ax bx cx dx es, x_center: word, y_center:word, radius:word, limitY:word, limitX:word, quadraunts:word
	local x:word, y:word, gap:word, delta:word
	mov ax, x
	add ax, limitX
	mov x,ax
	
	mov ax, y_center
	mov y,ax
	
	loopMark:
		mov ax,y
		sub ax, limitY
		jl endLoopMark
		  mov ah,02h
		  mov dx,x_center
		  int 21h
		quadtrant1:
			mov ax, quadraunts
			and ax, quadtrant1Mask
			shl ax, 3
			sub ax, 1
			;jne quadtrant2
			
			mov bx,y_center
			add bx,y
			push ax
			mov bx, x_center
			add bx, x
			push bx
			call draw_point
			
		quadtrant2:
		quadtrant3:
		quadtrant4:
		
		; gap = 2 * (delta + y) - 1;
		mov ax, delta
		add ax, y
		shr ax, 1
		sub ax, 1
		mov gap,ax
		
		;if (delta < 0 && gap <= 0)
		mov bx,delta
		cmp bx, 0
		jge deltaPositive
		
		cmp ax,0
		jg deltaPositive
		fix_error:
			mov cx,x
			inc cx
			mov x,cx
			shr cx,1
			inc cx
			add cx, delta
			mov delta,cx
			jmp loopMark
		
		deltaPositive:		
		;if (delta > 0 && gap > 0)
		mov bx,delta
		cmp bx,0
		jle normal_reaction
		
		cmp ax,0
		jle normal_reaction
		fix_error2:
			mov cx,y
			dec cx
			mov y,cx
			shr cx,1
			inc cx
			mov dx, delta
			sub dx,cx
			mov delta,dx
			jmp loopMark
		
		normal_reaction:
			;x++
			mov ax,x
			inc ax
			mov x,ax
			
			;delta += 2 * (x - y);
			mov ax,x
			sub ax,y
			shr ax,1
			add bx,ax
			mov delta,bx
			
			mov ax,y
			dec ax
			mov y,ax
			
			jmp loopMark
		
		
	endLoopMark:
	ret
draw_circle endp

draw_point proc uses ax bx cx dx es, x:word, y:word

	mov  dx,3ceh 	;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	;---рисуем точку с кодом цвета 1101b по нужному адресу
   	mov  ax,0a000h    ;указываем на видеобуфер
   	mov  es,ax        ;

	xor 	bx, bx		;обнуляем регистр адреса 
	mov	ax,y		;координата y
	mov	dx,x            ;значение координаты x
	shl	ax,4			;умножаем y на 16	
	add	bx,ax			;добавляем к адресу значение (y)*16
	shl	ax,2			;умножаем y на 64
	add	bx,ax			;добавляем к адресу значение (y)*64
	shr	dx,3			;делим х на 8	
	add	bx,dx			;добавляем к адресу значение x/8 
	
	mov	ah, 10000000b	;маска 
	mov	cx, x		;значение 
	and  	cl, 00000111b	;получение остатка от деления x на 8
	shr 	ah, cl		;сдвигаем маску на остаток
	
   	;---маскируем нужный бит
   	mov  dx, 03ceh       	;указываем на адресный регистр
   	mov  al, 8           	;номер регистра
   	out  dx, al          	;посылаем его
   	inc  dx	         		;указываем на регистр данных
   	mov  al, ah          	;маска
   	out  dx, al          	;посылаем данные
	;---чистим текущее содержимое задвижки
   	mov  al,es:[bx]     	;читаем содержимое в задвижку
   	mov  al,0           	;готовимся к очистке
   	mov  es:[bx],al     	;чистим задвижку
	;---установка регистра маски карты для заданного цвета 
   	mov  dx,03c4h       	;указываем на адресный регистр
   	mov  al,2           	;индекс регистра маски карты
   	out  dx,al          	;установка адреса
   	inc  dx             	;указываем на регистр данных
   	mov  al,color       	;код цвета
   	out  dx,al          	;посылаем код цвета
	;---рисуем точку
   	mov  al,0ffh        	;значение со всеми установленными битами
   	mov  es:[bx],al     	;выводим точки
	
	ret
draw_point endp

main:
	mov ax,@data
	mov ds,ax

	mov ax, 010h
	int 10h

	mov ax,0a000h      ;указываем на видеобуфер
	mov es,ax          ;

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	push 0Fh 			;квадранты
	push 0				; limitX
	push 0				; limitY
	push 10				; радиус
	push 10				; y_center
	push 10				; x_center
	call draw_circle
	
		
	xor ax,ax			;ожидание нажатия клавиши
	int 16h

	mov ax,4c00h		;выход из графики с возвратом
	int 21h				;в предыдущий режим
end main