.286
.model  small, c
.stack 256
.data
				  
   	defaultColor 	 	equ 1011b		;код цвета - ярко-красный	
   	earsColor 	 		equ 1010b		;код цвета - ярко-красный
	noseColor			equ 1010b
	eyesColor			equ 1010b
	eyesColor			equ 1010b
	quadtrant1Mask 	 	equ 08h  		;маски для квадрантов
	quadtrant2Mask 	 	equ 04h
    quadtrant3Mask  	equ 02h
    quadtrant4Mask 		equ 01h
	
	leftByteMask 		db 255,127,63,31,15,7,3,1
	rightByteMask 		db 10000000b
						db 11000000b
						db 11100000b
						db 11110000b
						db 11111000b
						db 11111100b
						db 11111110b
						db 11111111b				  
	twoBytes equ 0x01
	oneByte  equ 0h
.code

draw_circle proc uses ax bx cx dx es si di,  x_center: word, y_center:word, radius:word, limitY:word, limitX:word, quadraunts:word, color:word
	local x:word, y:word, gap:word, delta:word
	
	mov ax, 0
	add ax, limitX
	mov x,ax
	
	mov ax, radius
	mov y,ax	
	
	mov ax,0
	mov gap,ax
	
	mov ax,radius
	shl radius,1
	mov bx,2
	sub bx,ax
	mov delta,bx
	
	loopMark:
		mov ax,y	
		sub ax, limitY
		jl endLoopMark
		
		quadtrant1:
			mov ax, quadraunts
			and ax, quadtrant1Mask
			shr ax, 3
			cmp ax, 1
			jne quadtrant2
			
			push color
			mov bx,y_center
			sub bx,y
			push bx
			mov bx, x_center
			add bx, x
			push bx
			call draw_point
			
		quadtrant2:
			mov ax, quadraunts
			and ax, quadtrant2Mask
			shr ax, 2
			cmp ax, 1
			jne quadtrant3
			
			push color
			mov bx,y_center
			sub bx,y
			push bx
			mov bx, x_center
			sub bx, x
			push bx
			call draw_point
			
		quadtrant3:	
			mov ax, quadraunts
			and ax, quadtrant3Mask
			shr ax, 1
			cmp ax, 1
			jne quadtrant4
			
			push color
			mov bx,y_center
			add bx,y
			push bx
			mov bx, x_center
			sub bx, x
			push bx
			call draw_point
			
		quadtrant4:
			mov ax, quadraunts
			and ax, quadtrant4Mask
			cmp ax, 1
			jne fixing_erros
				
			push color
			mov bx,y_center
			add bx,y
			push bx
			mov bx, x_center
			add bx, x
			push bx
			call draw_point
		
		fixing_erros:
		; gap = 2 * (delta + y) - 1;
		mov ax, delta
		add ax, y
		shl ax, 1
		sub ax, 1
		mov gap,ax
		
		;if (delta < 0 && gap <= 0)
		mov bx,delta
		cmp bx, 0
		jge deltaPositive
		
		cmp ax,0
		jg deltaPositive
		fix_error:
			;x++
			mov cx,x
			inc cx
			mov x,cx
			;delta += 2 * x + 1;
			shl cx,1
			inc cx
			add cx, delta
			mov delta,cx
			jmp loopMark
		
		deltaPositive:		
		;if (delta > 0 && gap > 0)
		mov bx, delta
		cmp bx,0
		jle normal_reaction
		
		cmp ax,0
		jle normal_reaction
		fix_error2:
			;y--
			mov cx,y
			dec cx
			mov y,cx
			;delta -= 2 * x + 1;
			shl cx,1
			inc cx
			mov dx, delta
			sub dx,cx
			mov delta,dx
			jmp loopMark
		
		normal_reaction:
			;x++
			mov ax,x
			inc ax
			mov x,ax
			
			;delta += 2 * (x - y);
			mov ax,x
			sub ax,y
			shl ax,1
			mov bx,delta
			add bx,ax
			mov delta,bx
			
			;y--
			mov ax,y
			dec ax
			mov y,ax
			
			jmp loopMark
		
		
	endLoopMark:
	ret
draw_circle endp

draw_point proc uses ax bx cx dx es di, x:word, y:word, color:word

	mov  dx,3ceh 	;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	;---рисуем точку с кодом цвета 1101b по нужному адресу
   	mov  ax,0a000h    ;указываем на видеобуфер
   	mov  es,ax        ;

	xor 	bx, bx		;обнуляем регистр адреса 
	mov	ax,y		;координата y
	mov	dx,x            ;значение координаты x
	shl	ax,4			;умножаем y на 16	
	add	bx,ax			;добавляем к адресу значение (y)*16
	shl	ax,2			;умножаем y на 64
	add	bx,ax			;добавляем к адресу значение (y)*64
	shr	dx,3			;делим х на 8	
	add	bx,dx			;добавляем к адресу значение x/8 
	
	mov	ah, 10000000b	;маска 
	mov	cx, x		;значение 
	and  	cl, 00000111b	;получение остатка от деления x на 8
	shr 	ah, cl		;сдвигаем маску на остаток
	
   	;---маскируем нужный бит
   	mov  dx, 03ceh       	;указываем на адресный регистр
   	mov  al, 8           	;номер регистра
   	out  dx, al          	;посылаем его
   	inc  dx	         		;указываем на регистр данных
   	mov  al, ah          	;маска
   	out  dx, al          	;посылаем данные
	;---чистим текущее содержимое задвижки
   	mov  al,es:[bx]     	;читаем содержимое в задвижку
   	mov  al,0           	;готовимся к очистке
   	mov  es:[bx],al     	;чистим задвижку
	;---установка регистра маски карты для заданного цвета 
   	mov  dx,03c4h       	;указываем на адресный регистр
   	mov  al,2           	;индекс регистра маски карты
   	out  dx,al          	;установка адреса
   	inc  dx             	;указываем на регистр данных
   	mov  ax,color       	;код цвета
   	out  dx,al          	;посылаем код цвета
	;---рисуем точку
   	mov  al,0ffh        	;значение со всеми установленными битами
   	mov  es:[bx],al     	;выводим точки
	
	ret
draw_point endp

draw_filled_circle proc uses ax bx cx dx es si di,  x_center: word, y_center:word, radius:word, color:word
	local x:word, y:word, gap:word, delta:word
	
	
	mov ax, 0
	mov x,ax
	
	mov ax, radius
	mov y,ax	
	
	mov ax,0
	mov gap,ax
	
	mov ax,radius
	shl radius,1
	mov bx,2
	sub bx,ax
	mov delta,bx
	
	loopMark:
		mov ax,y	
		cmp ax, 0
		jl endLoopMark
				
			;рисуем линию
			push color
			;y
			mov cx, y_center
			sub cx,y
			push cx
			;x2
			mov cx, x_center
			add cx, x
			push cx 
			;x1
			mov cx, x_center
			sub cx, x
			push cx 
			call draw_hor_line
			
			push color
			;y
			mov cx, y_center
			add cx,y
			push cx
			;x2
			mov cx, x_center
			add cx, x
			push cx 
			;x1
			mov cx, x_center
			sub cx, x
			push cx 
			call draw_hor_line
			
		fixing_erros:
		; gap = 2 * (delta + y) - 1;
		mov ax, delta
		add ax, y
		shl ax, 1
		sub ax, 1
		mov gap,ax
		
		;if (delta < 0 && gap <= 0)
		mov bx,delta
		cmp bx, 0
		jge deltaPositive
		
		cmp ax,0
		jg deltaPositive
		fix_error:
			;x++
			mov cx,x
			inc cx
			mov x,cx
			;delta += 2 * x + 1;
			shl cx,1
			inc cx
			add cx, delta
			mov delta,cx
			jmp loopMark
		
		deltaPositive:		
		
		;if (delta > 0 && gap > 0)
		mov bx, delta
		cmp bx,0
		jle normal_reaction
		
		cmp ax,0
		jle normal_reaction
		fix_error2:
			;y--
			mov cx,y
			dec cx
			mov y,cx
			;delta -= 2 * x + 1;
			shl cx,1
			inc cx
			mov dx, delta
			sub dx,cx
			mov delta,dx
						
			jmp loopMark
			
		normal_reaction:
			;x++
			mov ax,x
			inc ax
			mov x,ax
			
			;delta += 2 * (x - y);
			mov ax,x
			sub ax,y
			shl ax,1
			mov bx,delta
			add bx,ax
			mov delta,bx
			
			;y--
			mov ax,y
			dec ax
			mov y,ax
			
			jmp loopMark
		
		
	endLoopMark:
	ret
draw_filled_circle endp


;Строит горизонтальную линию
draw_hor_line proc uses ax bx cx dx es si di, x1:word, x2:word, y:word, color:word
	
	; проверка аргументов на корректность
	mov ax,x1
	mov bx,x2
	cmp bx,ax
	jl exchange
	jmp correctArgs
	exchange:
		mov ax, x1
		mov bx, x2
		mov x1, bx
		mov x2, ax
	correctArgs:
	
	xor ax,ax
	xor bx,bx
		
	;---установка регистра маски карты для цвета 1101b
	mov dx,03c4h       	;указываем на адресный регистр
	mov al,2           	;индекс регистра маски карты
	out dx,al          	;установка адреса
	inc dx             	;указываем на регистр данных
	mov ax,color       	;код цвета
	out dx,al          	;посылаем код цвета
	
	;--- определяем байтовое смещение x1
	xor bx, bx	  ; обнуляем регистр адреса	
		
	mov ax, y
	mov dx, x1
	mov	cx, dx			;значение x
	shl	ax,4			; y<<4
	add	bx,ax			; adr+=y<<4
	shl	ax,2			; (y<<4)<<2
	add	bx,ax			; adr+=(y<<4)<<2
	shr	dx,3			; х>>3
	add	bx,dx			; adr+=x>>3
	push bx				; сохраним байтовое смещение x1 
	
	;--- определяем байтовое смещение x2
	xor bx, bx	  ; обнуляем регистр адреса	
		
	mov ax, y
	mov dx, x2
	mov	cx, dx			;значение x
	shl	ax,4			; y<<4
	add	bx,ax			; adr+=y<<4
	shl	ax,2			; (y<<4)<<2
	add	bx,ax			; adr+=(y<<4)<<2
	shr	dx,3			; х>>3
	add	bx,dx			; adr+=x>>3
	
	;-- достаем байтовое смещение x1
	pop ax
	; находим разность, т.е. количество байт
	sub bx, ax
	
	; сохраняем количество байт и начальное смещение для будущего
	push bx 
	push ax 
	
	; определяем, какая у нас ситуация
	jz inSameByte
	sub bx, 01h
	jz twoNearBytes
	jmp moreThanTwo
	
	;--- все точки в одном байте
	inSameByte:			
		mov  dx, 03ceh       	;указываем на адресный регистр
		mov  al, 8           	;номер регистра
		out  dx, al          	;посылаем его
		inc  dx  	        	;указываем на регистр данных
		
		;--- формируем маску
		mov cx, x1
		mov	ah, 10000000b	;маскa
		and cl, 00000111b	;получаем остаток от деления x на 8
		shr ah, cl			;сдвигаем маску на удвоенный остаток
		mov bh, ah
		
		mov dx, x2
		sub dx, x1
		jz onePixel
		
		mov cx, x2
		sub cx, x1
		sameByteMask:
			shr bh,1 
			or ah, bh
			loop sameByteMask
		
		onePixel:
		
		;---маскируем нужные биты
		mov  dx, 03ceh       	;указываем на адресный регистр
		mov  al, 8           	;номер регистра
		out  dx, al          	;посылаем его
		inc  dx  	        	;указываем на регистр данных
		mov  al, ah          	;маска
		out  dx, al          	;посылаем данные
				
		pop  bx
			
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
		
		;---чистим текущее содержимое задвижки
		mov  al,es:[bx]     	;читаем содержимое в задвижку
		mov  al,0           	;готовимся к очистке
		mov  es:[bx],al     	;чистим задвижку
		;---рисуем точку
		mov  al,0ffh        	;значение со всеми установленными битами
		mov  es:[bx],al     	;выводим точки
		add  bx, 050h	   		;переходим на следующую
		

		jmp finish
	;--- точки в двух последовательных байтах
	twoNearBytes:
		
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
	
		pop bx
		
		; загружаем маску
		MOV dx, x1 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, leftByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		inc bx
		
		; загружаем маску
		MOV dx, x2 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, rightByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		jmp finish	
	
	;--- точки в 3 бай и более	
	moreThanTwo:
		mov  ax,0a000h    ;указываем на видеобуфер
		mov  es,ax        ;
	
		pop bx
		
		;--- закрашиваем левый байт
		MOV dx, x1 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, leftByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
		inc bx
				
		;---все центральные байты
		pop cx
		
		mov di, bx ; устанавливаем смещение на начало
		mov al, 0ffh ; будем заполнять
		rep stosb
		
		;закрашиваем правый байт
		mov bx, di
		MOV dx, x2 ; загружаем битовое смещение
		and dx, 0111b
		XOR DH, DH ; очищаем старший байт регистра DX
		MOV SI, DX ; загружаем индексный регистр
		MOV AL, rightByteMask+[SI] ; загружаем маску в AL
				
		;---чистим текущее содержимое задвижки
		mov  ah,es:[bx]     	;читаем содержимое в задвижку
		mov  ah,0           	;готовимся к очистке
		mov  es:[bx],ah     	;чистим задвижку
		;---рисуем точку
		mov  ah,al        	;значение со всеми установленными битами
		mov  es:[bx],ah     	;выводим точки
	
	finish:
		ret
draw_hor_line endp


main:
	mov ax,@data
	mov ds,ax

	mov ax, 010h
	int 10h

	mov ax,0a000h      ;указываем на видеобуфер
	mov es,ax          ;

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	
	push defaultColor
	push 30
	push 100
	push 250
	call draw_hor_line
	
	; рисуем уши
	push defaultColor
	push 50			; радиус
	push 150				; y_center
	push 300			; x_center
	call draw_filled_circle
	
	; рисуем голову
	push defaultColor
	push 0Fh
	push 0
	push 0
	push 40				; радиус
	push 70			; y_center
	push 320				; x_center
	call draw_circle
		
		
	push defaultColor
	push 10
	push 100
	push 250
	call draw_hor_line
		
	xor ax,ax			;ожидание нажатия клавиши
	int 16h

	mov ax,4c00h		;выход из графики с возвратом
	int 21h				;в предыдущий режим
end main