
draw_vert_line proc
		
	mov di, y2
	mov ax, y1
	sub di, ax
	mov dx, x
	mov	cx, dx			;значение x
	shl	ax,4			; y<<4
	add	bx,ax			; adr+=y<<4
	shl	ax,2			; (y<<4)<<2
	add	bx,ax			; adr+=(y<<4)<<2
	shr	dx,3			; х>>3
	add	bx,dx			; adr+=x>>3

	mov	ah, 10000000b	;маскa
	and 	cl, 00000111b	;получаем остаток от деления x на 8
	shr 	ah, cl		;сдвигаем маску на удвоенный остаток
		
   	;---маскируем нужный бит
   	mov  dx, 03ceh       	;указываем на адресный регистр
   	mov  al, 8           	;номер регистра
   	out  dx, al          	;посылаем его
   	inc  dx  	        ;указываем на регистр данных
   	mov  al, ah          	;маска
   	out  dx, al          	;посылаем данные
	
	
	;---установка регистра маски карты для цвета 1101b
	mov  dx,03c4h       	;указываем на адресный регистр
	mov  al,2           	;индекс регистра маски карты
	out  dx,al          	;установка адреса
	inc  dx             	;указываем на регистр данных
	mov  al,color       	;код цвета
	out  dx,al          	;посылаем код цвета
	
	mov 	cx, di		;длина линии
	cycle:
		;---чистим текущее содержимое задвижки
		mov  al,es:[bx]     	;читаем содержимое в задвижку
		mov  al,0           	;готовимся к очистке
		mov  es:[bx],al     	;чистим задвижку
		;---рисуем точку
		mov  al,0ffh        	;значение со всеми установленными битами
		mov  es:[bx],al     	;выводим точки
		add  bx, 050h	   	;переходим на следующую
	loop cycle		;строку

	exit:	
		ret;
draw_vert_line endp;