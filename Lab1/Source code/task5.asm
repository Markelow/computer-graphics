.286
.model  small, C
.stack 256
.data
				  
   	color 	 equ 1100b		;код цвета - ярко-красный
.code

;Строит горизонтальную линию
draw_line proc x1:word, x2:word, y1:word, y2:word
	
	; Проверяем рост отрезка по оси икс и по оси игрек
	mov bx,y2
	mov ax,y1
	sub bx,ax
	
	push bx
	call math_abs
	mov bx, ax
	
	mov ax,x2
	mov cx,x1
	sub ax,cx
	
	push ax
	call math_abs
	
	sub bx,ax
	jg y_steep
	jmp x_steep
	y_steep:
		mov dl,1
		jmp swap_on_steep
	x_steep:
		mov dl,0
		jmp no_swap_on_steep
	
	swap_on_steep:
		mov ax,x1
		mov bx,y1
		mov x1,bx
		mov y1,ax
		mov ax,x2
		mov bx,y2
		mov x2,bx
		mov y2,ax
	no_swap_on_steep:
	
	;save steep to stack
	mov bp,sp
	push dx
	
	swap_if_x_incorrect:
		mov ax,x2
		sub ax,x1
		jng swap_on_x_incorrect
		jmp no_swap_on_x_incorrect
		swap_on_x_incorrect:
			mov ax,x1
			mov bx,x2
			mov x1,bx
			mov x2,ax
			mov ax,y1
			mov bx,y2
			mov y1,bx
			mov y2,ax
		no_swap_on_x_incorrect:
		
	; dx
	mov ax, x2
	sub ax,x1
	push ax
	
	; dy
	mov ax,y2
	sub ax,y1
	push ax
	
	mov dx,y1
	sub dx,y2
	jl posit_step
	jmp negate_step
		posit_step:
			mov dx,1
			jmp loop_prepare
		negate_step:
			mov dx,0
			dec dx
	
	loop_prepare:
	push dx
	
	; mov ax,x1
	; mov bx,y1
	; mov cx,x2
	; sub cx,x1
	; mov dx,[bp+2]
	; shr dx,1
	; push dx
	; draw_loop:
		; mov dx,[bp]
		; cmp dx,1
		; je high_steep
		; jmp low_steep
		; high_steep:
			; push ax
			; push bx
			; call draw_point
		; low_steep:
			; push bx
			; push ax
			; call draw_point
		; mov dx,[bp+8]
		; sub dx, [bp+4]
		; cmp dx,0
		; jl fix_error
		; jmp no_error
		; fix_error:
			; add bx,[bp+6]
			; add dx,[bp+2]
			; mov [bp+8],dx
		; no_error:
		
		; inc ax
	; loop draw_loop;

	finish:
		ret
draw_line endp

math_abs proc num:word

	mov ax, num
	abs:
	neg ax
	js abs ; переход на метку если выставлен флаг SF (то есть если число отрицательное)
		
	ret 
math_abs endp

draw_point proc uses ax bx cx dx es, x:word, y:word
	;--определяем байт
   	mov ax, 0A000H    	;указываем на видеобуфер
   	mov es, ax        	
	
	xor bx, bx			;обнуляем регистр адреса 
	mov	ax, y			;координата y
	mov	dx, x           ;координата x
	shl	ax, 4			;y*16	
	add	bx, ax			;adr+=(y)*16
	shl	ax, 2			;(y*16)*4
	add	bx, ax			;adr+=(y*16)*4
	shr	dx, 3			;х/8	
	add	bx, dx			;adr+=x/8 
	
	mov	ah, 10000000b	;маска 
	mov	cx, x			;значение 
	and cl, 00000111b	;получение остатка от деления x на 8
	shr ah, cl			;сдвигаем маску на остаток
	
   	;---маскируем нужный бит
   	mov dx, 03ceh       ;указываем на адресный регистр
   	mov al, 8           ;номер регистра
   	out dx, al          ;посылаем его
   	inc dx	         	;указываем на регистр данных
   	mov al, ah          ;маска
   	out dx, al          ;посылаем данные
	
	;--чистим содержимое нужных битов задвижки
   	mov al, es:[bx]     ;читаем содержимое в задвижку
    xor al, al	        ;готовимся к очистке 		
   	mov es:[bx],al      ;чистим задвижку
	
	;--установка регистра маски карты для цвета
   	mov dx, 03C4H       ;указываем на адресный регистр
   	mov al, 2           ;индекс регистра маски карты
   	out dx, al          ;установка адреса
   	inc dx              ;указываем на регистр данных
   	mov al, color       ;код цвета - яркий циан
   	out dx, al          ;посылаем код цвета
	
	;--рисуем точку
   	mov ax, 0FFH       	;любое значение с установленными 
   	mov es:[bx],ax    	;выводим точки
	
	ret
draw_point endp

main:
	mov ax,@data
	mov ds,ax

	mov ax, 010h
	int 10h

	mov ax,0a000h      ;указываем на видеобуфер
	mov es,ax          ;

	mov  dx,3ceh 		;указываем на регистр адреса
   	mov  al,5       	;инедксируем регистр 5
   	out  dx,al      	;посылаем индекс
   	inc  dx         	;указываем на регистр режима
   	mov  al,0       	;выбираем режим записи 0
   	out  dx,al      	;устанавливаем режим
	
	
	push 4			;y2
	push 9			;y1
	push 1			;x2
	push 2			;x1
	call draw_line
	
	
	
	xor ax,ax			;ожидание нажатия клавиши
	int 16h

	mov ax,4c00h		;выход из графики с возвратом
	int 21h				;в предыдущий режим
end main