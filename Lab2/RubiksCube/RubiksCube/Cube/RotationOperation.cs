﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;

namespace RubiksCube.Cube
{
    internal class RotationOperation
    {
        public Action<TimeSpan> Method { get; set; }
        public TimeSpan AnimationTime { get; set; }
    }
}