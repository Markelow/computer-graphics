﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using RubiksCube.Logs;

namespace RubiksCube.Cube
{
    public class RubiksCubeRotationController
    {
        private readonly TimeSpan _defaultAnimationTime = new TimeSpan(0,0,0,0,500);

        public static RubiksCubeRotationController Instance
        {
            get { return _instance ?? (_instance = new RubiksCubeRotationController()); }
        }
        private static RubiksCubeRotationController _instance;

        private readonly ConcurrentQueue<RotationOperation> _queue;

        private readonly object _lockObject = new object();

        private RubiksCubeRotationController()
        {
            _queue = new ConcurrentQueue<RotationOperation>();
        }

        public void AddRotationOperation(Action<TimeSpan> rotationMethod, TimeSpan animationTime = default(TimeSpan))
        {
            if (animationTime == default(TimeSpan))
            {
                animationTime = _defaultAnimationTime;
            }
            var rotationOperation = new RotationOperation
            {
                Method = rotationMethod,
                AnimationTime = animationTime
            };
            AddRotationOperation(rotationOperation);
        }

        private void AddRotationOperation(RotationOperation rotationOperation)
        {
            if (_queue.IsEmpty)
            {
                try
                {
                    Task.Factory.StartNew(() =>
                    {
                        lock (_lockObject)
                        {
                            if (!_queue.IsEmpty)
                            {
                                _queue.Enqueue(rotationOperation);
                                return;
                            }
                            _queue.Enqueue(rotationOperation);
                        
                            RotationOperation localRotaionOperation;
                            while (_queue.TryDequeue(out localRotaionOperation))
                            {
                                var rotationTime = localRotaionOperation.AnimationTime;
                                localRotaionOperation.Method(rotationTime);
                            }
                        }
                    });
                }
                catch (Exception ex)
                {
                    Logger.Instance.LogError("RubiksCubeRotationController", ex);
                }
            }
            else
            {
                lock (_lockObject)
                {
                    _queue.Enqueue(rotationOperation);
                }
            }
        }
    }
}