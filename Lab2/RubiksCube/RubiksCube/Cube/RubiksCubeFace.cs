﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubiksCube.Cube
{
    public class RubiksCubeFace
    {
        private readonly HashSet<SmallCube> _cubes;

        public CubeFaceTriada UpTriada { get; set; }
        public CubeFaceTriada DownTriada { get; set; }
        public CubeFaceTriada LeftTriada { get; set; }
        public CubeFaceTriada RightTriada { get; set; }
        public CubeFaceTriada MiddleVerticalTriada { get; set; }
        public CubeFaceTriada MiddleHorizontalTriada { get; set; }

        public RubiksCubeFace()
        {
            _cubes = new HashSet<SmallCube>();
            UpTriada = new CubeFaceTriada();
            DownTriada = new CubeFaceTriada();
            LeftTriada = new CubeFaceTriada();
            RightTriada = new CubeFaceTriada();
            MiddleVerticalTriada = new CubeFaceTriada();
            MiddleHorizontalTriada = new CubeFaceTriada();
        }

        public void AddToUpTriada(SmallCube smallCube)
        {
            UpTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromUpTriada(SmallCube smallCube)
        {
            UpTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }

        public void AddToDownTriada(SmallCube smallCube)
        {
            DownTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromDownTriada(SmallCube smallCube)
        {
            DownTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }

        public void AddToLeftTriada(SmallCube smallCube)
        {
            LeftTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromLeftTriada(SmallCube smallCube)
        {
            LeftTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }

        public void AddToRightTriada(SmallCube smallCube)
        {
            RightTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromRightTriada(SmallCube smallCube)
        {
            RightTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }

        public void AddToMiddleVerticalTriada(SmallCube smallCube)
        {
            MiddleVerticalTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromMiddleVerticalTriada(SmallCube smallCube)
        {
            MiddleVerticalTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }

        public void AddToMiddleHorizontalTriada(SmallCube smallCube)
        {
            MiddleVerticalTriada.AddSmallCube(smallCube);
            if (!_cubes.Contains(smallCube))
            {
                _cubes.Add(smallCube);
            }
        }

        public void RemoveFromMiddleHorizontalTriada(SmallCube smallCube)
        {
            MiddleHorizontalTriada.RemoveSmallCube(smallCube);
            if (_cubes.Contains(smallCube))
            {
                _cubes.Remove(smallCube);
            }
        }
    }
}
