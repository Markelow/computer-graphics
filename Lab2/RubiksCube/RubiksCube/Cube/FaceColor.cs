﻿namespace RubiksCube.Cube
{
    public enum FaceColor
    {
        Default,
        Yellow,
        Green,
        Red,
        Blue,
        White, 
        Orange
    }
}
