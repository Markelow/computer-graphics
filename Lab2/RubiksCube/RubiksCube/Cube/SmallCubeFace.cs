﻿using RubiksCube.Drawing;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Assets;
using Color = System.Windows.Media.Color;

namespace RubiksCube.Cube
{
    public class SmallCubeFace: IDrawable
    {
        public static readonly Color LineColor = Color.FromRgb(200,200,200);
        public static readonly Color DefaultColor = Color.FromRgb(240, 240, 240);

        private Vertex _center;
        private Color _color;
        private readonly Texture _texture;

        private static Vertex _topLeft;
        private static Vertex _topRight;
        private static Vertex _bottomLeft;
        private static Vertex _bottomRight;

        
        public SmallCubeFace(Vertex ceneter, FaceColor color)
        {
            _center = ceneter;
            _topLeft = new Vertex(-1.0f, 1.0f, 0.0f);
            _topRight = new Vertex(1.0f, 1.0f, 0.0f);
            _bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
            _bottomRight = new Vertex(1.0f, -1.0f, 0.0f);

            _color = DefaultColor;
            _texture = SmallCubeFaceTextureMapper.Instance.GetTexture(color);
        }

        public void Draw(OpenGL openGl)
        {
            openGl.PushMatrix();

            openGl.Translate(_center.X, _center.Y, _center.Z);
            if (_texture == null)
            {
                DrawWithoutTexture(openGl);
            }
            else
            {
                DrawWithoutTexture(openGl);
                DrawWithTexture(openGl);
            }

            openGl.PopMatrix();
        }

        private void DrawWithTexture(OpenGL openGl)
        {
            openGl.Enable(OpenGL.GL_TEXTURE_2D);

            _texture.Bind(openGl);
            openGl.Begin(BeginMode.Quads);

            openGl.TexCoord(0.0f, 1.0f);
            openGl.Vertex(_topLeft);  // Слева вверху

            openGl.TexCoord(1.0f, 1.0f);
            openGl.Vertex(_topRight);  // Справа вверху

            openGl.TexCoord(1.0f, 0.0f);
            openGl.Vertex(_bottomRight);  // Справа внизу

            openGl.TexCoord(0.0f, 0.0f);
            openGl.Vertex(_bottomLeft);  // Слева внизу

            openGl.End();
            openGl.EndList();
            openGl.Disable(OpenGL.GL_TEXTURE_2D);
        }


        private void DrawWithoutTexture(OpenGL openGl)
        {
            openGl.Begin(BeginMode.Quads);
            openGl.Color(_color.R, _color.G, _color.B);
            openGl.Vertex(_topLeft);  // Слева вверху
            openGl.Color(_color.R, _color.G, _color.B);
            openGl.Vertex(_topRight);  // Справа вверху
            openGl.Color(_color.R, _color.G, _color.B);
            openGl.Vertex(_bottomRight);  // Справа внизу
            openGl.Color(_color.R, _color.G, _color.B);
            openGl.Vertex(_bottomLeft);  // Слева внизу
            openGl.Color(1.0f,1.0f,1.0f,1.0f);
            openGl.End();

            openGl.Color(LineColor.R, LineColor.G, LineColor.B);
            openGl.Begin(BeginMode.LineLoop);
            openGl.Vertex(_topLeft);  // Слева вверху
            openGl.Vertex(_topRight);  // Справа вверху
            openGl.Vertex(_bottomRight);  // Справа внизу
            openGl.Vertex(_bottomLeft);  // Слева внизу
            openGl.End();

            openGl.Color(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }
}
