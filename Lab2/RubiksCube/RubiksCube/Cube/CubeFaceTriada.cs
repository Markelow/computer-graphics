﻿using System.Collections.Generic;

namespace RubiksCube.Cube
{
    public class CubeFaceTriada
    {
        private readonly List<SmallCube> _cubes;

        public CubeFaceTriada()
        {
            _cubes = new List<SmallCube>(3);
        }

        public void AddSmallCube(SmallCube cube)
        {
            _cubes.Add(cube);
        }

        public void RemoveSmallCube(SmallCube cube)
        {
            if (_cubes.Contains(cube))
            {
                _cubes.Remove(cube);
            }
        }
    }
}
