﻿using System.Collections.Generic;
using RubiksCube.Drawing;
using SharpGL.SceneGraph.Assets;

namespace RubiksCube.Cube
{
    public class SmallCubeFaceTextureMapper
    {
        private readonly Dictionary<FaceColor, string> _map;

        public static SmallCubeFaceTextureMapper Instance
        {
            get { return _instance ?? (_instance = new SmallCubeFaceTextureMapper()); }
        }
        private static SmallCubeFaceTextureMapper _instance;

        private SmallCubeFaceTextureMapper()
        {
            _map = new Dictionary<FaceColor, string>
            {
                {FaceColor.Green, "GreenFace"},
                {FaceColor.Yellow, "YellowFace"},
                {FaceColor.Blue, "BlueFace"},
                {FaceColor.Orange, "OrangeFace"},
                {FaceColor.Red, "RedFace"},
                {FaceColor.White, "WhiteFace"},
            };
        }

        public Texture GetTexture(FaceColor color)
        {
            var textureName = string.Empty;

            if (_map.ContainsKey(color))
            {
                textureName = _map[color];
            }

            return TextureController.Instance.GetTexture(textureName);
        }
    }
}
