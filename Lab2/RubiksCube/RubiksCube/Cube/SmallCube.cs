﻿using System;
using RubiksCube.Drawing;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph;

namespace RubiksCube.Cube
{
    public class SmallCube : IDrawable, IRotateble
    {
        private Vertex _center;

        private readonly SmallCubeFace _frontFace;
        private readonly SmallCubeFace _leftFace;
        private readonly SmallCubeFace _rightFace;
        private readonly SmallCubeFace _downFace;
        private readonly SmallCubeFace _upFace;
        private readonly SmallCubeFace _backFace;
    


        public double[] LocalModelViewMatrix { get; set; }

        public RotationAngles RotationAngles { get; set; }

        public bool IsModelViewMatrixNotNull
        {
            get { return LocalModelViewMatrix != null && LocalModelViewMatrix.Length > 0; }
        }

        public SmallCube(Vertex center, 
            FaceColor frontFaceColor = FaceColor.Default, FaceColor backFaceColor = FaceColor.Default,
            FaceColor leftFaceColor = FaceColor.Default, FaceColor rightFaceColor = FaceColor.Default,
            FaceColor upFaceColor = FaceColor.Default, FaceColor downFaceColor = FaceColor.Default)
        {
            _center = center;
            RotationAngles = new RotationAngles();

            _frontFace = new SmallCubeFace(new Vertex(0, 0, 0 - 1f), frontFaceColor);
            _backFace = new SmallCubeFace(new Vertex(0, 0, 0 + 1.0f), backFaceColor);
            _leftFace = new SmallCubeFace(new Vertex(0, 0, 0 + 1f), leftFaceColor);
            _rightFace = new SmallCubeFace(new Vertex(0, 0, 0 - 1f), rightFaceColor);
            _downFace = new SmallCubeFace(new Vertex(0, 0, 0 + 1f), upFaceColor);
            _upFace = new SmallCubeFace(new Vertex(0, 0, 0 - 1f), downFaceColor);
        }

        public void Draw(OpenGL openGl)
        {
            openGl.MatrixMode(MatrixMode.Modelview);
            openGl.PushMatrix();

            openGl.Rotate(RotationAngles.X, RotationAngles.Y, RotationAngles.Z);
    
            openGl.Translate(_center.X, _center.Y, _center.Z);
            _frontFace.Draw(openGl);
            _backFace.Draw(openGl);
            openGl.Rotate(90, 0, 1, 0);
            _leftFace.Draw(openGl);
            _rightFace.Draw(openGl);
            openGl.Rotate(90, 1, 0, 0);
            _upFace.Draw(openGl);
            _downFace.Draw(openGl);

            openGl.PopMatrix();
        }

    }
}
