﻿using System;
using System.Collections.Generic;
using System.Threading;
using RubiksCube.Drawing;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph;

namespace RubiksCube.Cube
{
    public class RubiksCube : IDrawable
    {
        private const int RotationAngle = 90;

        private List<SmallCube> _frontFaceCubes;
        private List<SmallCube> _backCubes;
        private List<SmallCube> _upCubes;
        private List<SmallCube> _downCubes;
        private List<SmallCube> _leftCubes;
        private List<SmallCube> _rightCubes;
        private List<SmallCube> _innerCubes;
        private List<SmallCube> _middleCubes;

        private List<SmallCube> cubes; 

        public RubiksCube()
        {
            cubes = new List<SmallCube>(27);
            var cubeCenter = new Vertex(0,0,0);
            _upCubes = new List<SmallCube>(9);
            _leftCubes = new List<SmallCube>(9);
            _middleCubes = new List<SmallCube>(9);
            _rightCubes = new List<SmallCube>(9);
            for (int x = -2; x <= 2; x += 2)
            {
                for (int y = -2; y <= 2; y += 2)
                {
                    for (int z = -2; z <= 2; z += 2)
                    {
                        var smallCube = new SmallCube(new Vertex(x, y, z),
                            FaceColor.Red, FaceColor.Orange, FaceColor.White, 
                            FaceColor.Yellow, FaceColor.Blue, FaceColor.Green);


                        switch (x)
                        {
                            case -2:
                                _rightCubes.Add(smallCube);
                                break;
                            case 0:
                                _middleCubes.Add(smallCube);
                                break;
                            case 2:
                                _leftCubes.Add(smallCube);
                                break;
                        }
                        if (y == 2)
                        {
                            _upCubes.Add(smallCube);
                        }
                    }
                }
            }

        }

        public void Draw(OpenGL openGl)
        {;

            foreach (var smallCube in _leftCubes)
            {
                openGl.PushMatrix();
                smallCube.Draw(openGl);
                openGl.PopMatrix();
            }
            foreach (var smallCube in _middleCubes)
            {
                openGl.PushMatrix();
                smallCube.Draw(openGl);
                openGl.PopMatrix();
            }
            foreach (var smallCube in _rightCubes)
            {
                openGl.PushMatrix();
                smallCube.Draw(openGl);
                openGl.PopMatrix();
            }
        }

        public void RotateLeftFaceLeft(TimeSpan animationTime)
        {
            var sleepTime = animationTime.Milliseconds / RotationAngle;
            for (double a = 0; a < RotationAngle; a++)
            {
                foreach (var smallCube in _leftCubes)
                {

                    smallCube.RotationAngles.X++;
                }
                Thread.Sleep(sleepTime);

            }
            //TODO поменять местами в face
        }

        public void RotateLeftFaceRight(TimeSpan animationTime)
        {
            var sleepTime = animationTime.Milliseconds / RotationAngle;
            for (double a = 0; a < RotationAngle; a++)
            {
                foreach (var smallCube in _leftCubes)
                {

                    smallCube.RotationAngles.X--;
                }
                Thread.Sleep(sleepTime);

            }
            //TODO поменять местами в face
        }
    }
}
