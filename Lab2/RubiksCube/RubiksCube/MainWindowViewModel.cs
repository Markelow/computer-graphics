﻿using System;
using System.Windows.Media;
using RubiksCube.Cube;
using RubiksCube.Drawing;
using RubiksCube.Mvvm;
using SharpGL;
using SharpGL.SceneGraph;
using RubiksCube.Cube;

namespace RubiksCube
{
    public class MainWindowViewModel : Notifier
    {
        #region Константы

        #endregion 

        #region Свойства

        private SimpleCommand _rotateLeftFaceLeftCommand;
        public SimpleCommand RotateLeftFaceLeftCommand
        {
            get { return _rotateLeftFaceLeftCommand ?? (_rotateLeftFaceLeftCommand =  new SimpleCommand
            {
                CanExecuteDelegate = x => true,
                ExecuteDelegate = x => RotateLeftFaceLeft()
            }); }
        }

        private SimpleCommand _rotateLeftFaceRightCommand;
        public SimpleCommand RotateLeftFaceRightCommand
        {
            get
            {
                return _rotateLeftFaceRightCommand ?? (_rotateLeftFaceRightCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => true,
                    ExecuteDelegate = x => RotateLeftFaceRight()
                });
            }
        }

        private Cube.RubiksCube _rubiksCube; 

#endregion

        public MainWindowViewModel()
        {
        }

        public void InitOpenGl(OpenGL openGl)
        {
            OpengGlController.Init(openGl);

            OpengGlController.Instance.InitOpenGl();
            TextureController.Instance.Init(openGl);

            _rubiksCube = new Cube.RubiksCube();
            OpengGlController.Instance.AddObject(_rubiksCube);

        }

        public void WindowResizeHandler(double width, double height)
        {
            OpengGlController.Instance.UpdateOpenGlOnWindowResize(width, height);
        }

        public void Draw()
        {
            OpengGlController.Instance.Draw();
        }

        private void RotateLeftFaceLeft()
        {
            RubiksCubeRotationController.Instance.AddRotationOperation(_rubiksCube.RotateLeftFaceLeft);
        }

        private void RotateLeftFaceRight()
        {
            RubiksCubeRotationController.Instance.AddRotationOperation(_rubiksCube.RotateLeftFaceRight);
        }

    }
}
