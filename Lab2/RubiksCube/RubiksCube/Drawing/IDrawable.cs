﻿using SharpGL;

namespace RubiksCube.Drawing
{
    public interface IDrawable
    {
        void Draw(OpenGL openGl);
    }
}
