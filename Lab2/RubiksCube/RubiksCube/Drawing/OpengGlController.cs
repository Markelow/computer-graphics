﻿using System;
using System.Collections.Generic;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph.Core;
using SharpGL.SceneGraph.Primitives;

namespace RubiksCube.Drawing
{
    public class OpengGlController
    {
        private readonly OpenGL _openGl;
        public OpenGL OpenGl
        {
            get { return _openGl; }
        }

        private readonly List<IDrawable> _drawableObjects;

        private readonly Axies _axies = new Axies();


        #region Singletone

        public static OpengGlController Instance
        {
            get
            {
                if (_opengGlController == null)
                {
                    throw new InvalidOperationException("_opengGlController");
                }
                return _opengGlController;
            }
        }

        private static OpengGlController _opengGlController;

        #endregion

        public static void Init(OpenGL opengGl)
        {
            _opengGlController = new OpengGlController(opengGl);
        }

        private OpengGlController(OpenGL opengGl)
        {
            if (opengGl == null) throw new ArgumentNullException("opengGl");

            _openGl = opengGl;
            _drawableObjects = new List<IDrawable>();
        }

        #region Управление OpenGl

        public void InitOpenGl()
        {
            //  Set the clear color.
            _openGl.ClearColor(0, 0, 0, 0);
            
            // Разрешить очистку буфера глубины
            _openGl.ClearDepth(1.0f);
            // Разрешить тест глубины
            _openGl.Enable(OpenGL.GL_DEPTH_TEST);

            // Тип теста глубины
             _openGl.DepthFunc(OpenGL.GL_LEQUAL); 
        }

        public void UpdateOpenGlOnWindowResize(double width, double height)
        {
            //  Set the projection matrix.
            _openGl.MatrixMode(OpenGL.GL_PROJECTION);

            //  Load the identity.
            _openGl.LoadIdentity();

            //  Create a perspective transformation.
            _openGl.Perspective(60.0f, width/height, 0.01, 100.0);

            //  Use the 'look at' helper function to position and aim the camera.
            _openGl.LookAt(-5, 5, -15, 0, 0, 0, 0, 1, 0);

            //  Set the modelview matrix.
            _openGl.MatrixMode(OpenGL.GL_MODELVIEW);
        }

        #endregion

        public void AddObject(IDrawable drawableObject)
        {
            if (drawableObject == null) throw new ArgumentNullException("drawableObject");

            _drawableObjects.Add(drawableObject);
        }

        public void AddObjects(IEnumerable<IDrawable> objects)
        {
            if (objects == null) throw new ArgumentNullException("objects");

            foreach (var drawable in objects)
            {
                if (drawable == null) continue;

                _drawableObjects.Add(drawable);
            }
        }

        public void RemoveObject(IDrawable drawableObject)
        {
            if (drawableObject == null) throw new ArgumentNullException("drawableObject");

            if (_drawableObjects.Contains(drawableObject))
            {
                _drawableObjects.Remove(drawableObject);
            }
        }

        public void Draw()
        {
            //  Clear the color and depth buffer.
            _openGl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            //  Load the identity matrix.
            _openGl.LoadIdentity();

            foreach (var drawable in _drawableObjects)
            {
                _openGl.PushMatrix();
                drawable.Draw(_openGl);
                _openGl.PopMatrix();
            } 

            _axies.Render(OpenGl, RenderMode.Design);
            RotateAxies(-0.5, 1, 0, 0);
            RotateAxies(0.5, 0, 1, 0);
        }

        public void Rotate(double angle, double x, double y, double z)
        {
            _openGl.Rotate(angle, 1.0f, 1.0f, 1.0f);
        }

        public void RotateAxies(double angle, double x, double y, double z)
        {
            _openGl.MatrixMode(MatrixMode.Projection);
            _openGl.Rotate(angle, x, y, z);
            _openGl.MatrixMode(MatrixMode.Modelview);
        }

        public void Rotate(double angle, Action<OpenGL, double> method)
        {
            method(_openGl, angle);
        }
    }
}
