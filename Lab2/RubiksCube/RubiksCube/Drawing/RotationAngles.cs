﻿namespace RubiksCube.Drawing
{
    public class RotationAngles
    {
        private readonly object _lockObject = new object();

        private int _x;
        public int X
        {
            get
            {
                lock (_lockObject) {return _x;}
            }
            set
            {
                lock (_lockObject)
                {_x = value;}
            }
        }

        private int _y;
        public int Y
        {
            get
            {
                lock (_lockObject){ return _y;}
            }
            set
            {
                lock (_lockObject) {_y = value;}
            }
        }
        private int _z;
        public int Z
        {
            get
            {
                lock (_lockObject) {return _z;}
            }
            set
            {
                lock (_lockObject) {_z = value;}
            }
        }


        private static float GetCorrectAngle(float value)
        {
            if (value > 0)
            {
                return value > 360
                    ? value - 360
                    : value;
            }
            return value < -360
                ? value + 360
                : value;
        }
    }
}
