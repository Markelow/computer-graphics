﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;
using SharpGL.SceneGraph;

namespace RubiksCube.Drawing
{
    public interface IRotateble
    {
        RotationAngles RotationAngles { get; set; }
    }
}
