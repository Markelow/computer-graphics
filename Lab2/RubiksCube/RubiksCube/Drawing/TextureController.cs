﻿using System;
using System.Collections.Generic;
using SharpGL;
using System.Configuration;
using System.IO;
using RubiksCube.Logs;
using SharpGL.SceneGraph.Assets;

namespace RubiksCube.Drawing
{
    public class TextureController
    {
        private const string TexturesDirectory = "TexturesLocation";
        
        private readonly Dictionary<string, Texture> _textures;
        public Dictionary<string, Texture> Textures
        {
            get { return _textures; }
        }

        public static TextureController Instance
        {
            get { return _textureController ?? (_textureController = new TextureController()); }
        }
        
        private static TextureController _textureController;

        private TextureController()
        {
            _textures = new Dictionary<string, Texture>();
        }

        public void Init(OpenGL openGl)
        {
            try
            {
                var texturesDir = ConfigurationManager.AppSettings[TexturesDirectory];
                texturesDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, texturesDir);
                var texturesDirectories = new List<string> {texturesDir};
                foreach (var directory in Directory.GetDirectories(texturesDir))
                {
                    texturesDirectories.Add(directory);
                }
                foreach (var directoryPath in texturesDirectories)
                {
                    foreach (var texturePath in Directory.GetFiles(directoryPath))
                    {
                        var textureName = Path.GetFileNameWithoutExtension(texturePath) ?? texturePath; 
                        openGl.Enable(OpenGL.GL_TEXTURE_2D);
                        var texture = new Texture();
                        texture.Create(openGl, texturePath);
                        _textures.Add(textureName, texture);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError("TextureController", ex);
            }
        }

        public Texture GetTexture(string textureName)
        {
            return _textures.ContainsKey(textureName)
                ? _textures[textureName]
                : null;

        }
    }
}
